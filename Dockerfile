
# Use the official Nginx image as the base image
FROM nginx

# Install SSH server and Supervisor for managing multiple processes
RUN apt-get update && apt-get install -y openssh-server supervisor

# Create SSH directory and generate keys
RUN mkdir /var/run/sshd && \
    echo 'root:password' | chpasswd && \
    sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config && \
    sed -i 's/#PasswordAuthentication yes/PasswordAuthentication yes/' /etc/ssh/sshd_config

# Copy supervisor configuration file
COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf

# Expose SSH and HTTP ports
EXPOSE 22
EXPOSE 80

# Start Supervisor to manage SSH and Nginx services
CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf"]

